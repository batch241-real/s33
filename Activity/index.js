console.log(fetch('https://jsonplaceholder.typicode.com/todos'));

// fetch all todo list items
async function fetchAllTodoList(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos',{
		method:'GET'
	});

	let json = await result.json();

	let jsonArr = json.map(function(json){
		let title = {'title': json.title}
		return title
	})
	console.log(jsonArr);

};
fetchAllTodoList();


// fetch single todo list item
async function fetchSingleTodoList(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method:'GET'
	});

	let json = await result.json();
	console.log(json);
	console.log(`The ${json.title} on the list has a status of ${json.completed}`);
};
fetchSingleTodoList();


// fetch and POST create todo list item
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		"userId": 1,
	    "title": "Create To Do List Item",
    	"completed": "false"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// fetch and PUT update todo list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
	    "title": "Update To Do List Item",
    	"description": "Update in different structure",
    	"Status": "pending",
    	"userId": 5,
    	"date completed": "pending"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// fetch and PATCH update todo list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		"title": "Update To Do List Item",
    	"description": "Update in different structure",
    	"Status": "complete",
    	"userId": 5,
    	"date completed": "January 31, 2023"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// fetch and DELETE
fetch('https://jsonplaceholder.typicode.com/todos/10', {method: 'DELETE'})